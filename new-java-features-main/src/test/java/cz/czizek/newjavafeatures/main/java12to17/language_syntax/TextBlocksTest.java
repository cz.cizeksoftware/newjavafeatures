package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class TextBlocksTest {
    // Text Blocks  aka  Raw String Literals

    @Test
    void stringAsTextBlock() {
        // Note: Opening fat delimiter (""") must be the last thing on the line - the text begins on the next line
        var str1 = """
                The first line with some "quotes" but without escaping them.
                And second line without any need for new-line""";
        var str2 = "The first line with some \"quotes\" but without escaping them.\nAnd second line without any need for new-line";
        assertThat(str1, equalTo(str2));
    }

    @Test
    void trimsTrailingWhitespacesFromEachLine() {
        // see  \s  escape sequence to prevent that
        var str1 = """
                This is a line with some trailing spaces                   
                And another line with trailing spaces   """;
        var str2 = "This is a line with some trailing spaces\nAnd another line with trailing spaces";
        assertThat(str1, equalTo(str2));
    }

    @Test
    void indents() {
        // Keeps indentation of individual lines
        var str1 = """
                <html>
                    <body>
                        <span>example text</span>
                    </body>
                </html>""";

        var str2 = String.join("\n",
                "<html>",
                "    <body>",
                "        <span>example text</span>",
                "    </body>",
                "</html>");
        assertThat(str1, equalTo(str2));

        // Indentation starts from (a)position of any left-most text or (b)position of closing fat delimiter ("""), whichever is first(closer to left side)
        str1 = """
                  line1
                  line2
                """;

        str2 = "  line1\n  line2\n";
        assertThat(str1, equalTo(str2));


        str1 = """
                line1
                    line2
                  """;
        str2 = "line1\n    line2\n"; // still trims trailing whitespaces so there's no space on the last line
        assertThat(str1, equalTo(str2));
    }

    @Test
    void escapes() {
        // \   wraps the written line without adding line-end (\n) to the string
        // \s  represents one space ( ) - useful as the last character of a line to avoid the trailing whitespaces trimming
        // \r  carriage return (Note: even if source file has Windows line-ends(/n/r), TextBlock lines always use just \n. If we need \r, we have to add it manually)
        // \t  tab
        // \"  quote character - generally bad practice to escape quotes in Text Blocks but useful to write three of them in a row (""")


        var str1 = """
                First line \
                still first line, only written down separately
                        
                We can also write a fat delimiter \""" by escaping one of the quotes
                        
                And a space at the end that's not going to be trimmed-off: \s""";

        var str2 = String.join("\n",
                "First line " + "still first line, only written down separately",
                "",
                "We can also write a fat delimiter \"\"\" by escaping one of the quotes",
                "",
                "And a space at the end that's not going to be trimmed-off:  ");
        assertThat(str1, equalTo(str2));
    }
}
