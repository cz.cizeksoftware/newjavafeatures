package cz.czizek.newjavafeatures.main.java8to11.language_syntax;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class SafeVarargsTest {
    @Test
    void safeVarargsAnnotationAtPrivateMethods() {
        assertThat(delistify(List.of("A", "B", "C"), List.of("X", "Y", "Z")), contains("ABC", "XYZ"));
    }

    // We ONLY READ ITEMS FROM stringListsArray and also IT NEVER LEAVES this method => we're safe to use Variable Arguments
    @SafeVarargs  // couldn't be at private methods before Java9
    private List<String> delistify(List<String>... stringListsArray) {
        return Stream.of(stringListsArray)
                .map(strings -> String.join("", strings))
                .collect(toList());
    }
}
