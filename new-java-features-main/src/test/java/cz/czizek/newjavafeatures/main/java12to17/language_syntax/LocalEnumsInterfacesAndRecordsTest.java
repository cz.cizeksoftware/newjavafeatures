package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.is;

public class LocalEnumsInterfacesAndRecordsTest {


    @Test
    void enumsInterfacesAndRecordsCanBeDeclaredLocally() {

        interface Named {
            String name();
        }


        enum Sex implements Named {MAN, WOMAN}

        record Point(int x, int y) {
        }

        assertThat(Sex.values(), is(arrayContaining(Sex.MAN, Sex.WOMAN)));
        assertThat(Sex.class.getInterfaces(), Matchers.hasItemInArray(Named.class));

        assertThat(Point.class.isRecord(), is(true));
    }
}
