package cz.czizek.newjavafeatures.main.java8to11.features;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class IndifyStringConcatenationTest {

    @Test
    void indifyStringConcatenation() {
        // Indify String concatenation

        // Concatenating Strings is now faster then using StringBuilder (and it's garbage-free) (and JVM can better optimize it in runtime than a StringBuilder/StringBuffer)
        //  ... which is good to know in loops or other hot code
        //  ... also, StringConcatFactory might be a subject for further optimizations in future JDK versions without need of even recompiling the code

        String helloClass = "vatapa" + " ňumba".repeat(2);
        // Is compiled to bytecode as:    (  javap -c -p IndifyStringConcatenationTest.class  )

        /*  For Java8  (uses StringBuilder)
       0: new           #7                  // class java/lang/StringBuilder
       3: dup
       4: invokespecial #9                  // Method java/lang/StringBuilder."<init>":()V
       7: ldc           #10                 // String vatapa
       9: invokevirtual #12                 // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
      12: ldc           #16                 // String  ňumba
      14: iconst_2
      15: invokevirtual #18                 // Method java/lang/String.repeat:(I)Ljava/lang/String;
      18: invokevirtual #12                 // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
      21: invokevirtual #24                 // Method java/lang/StringBuilder.toString:()Ljava/lang/String;
         */

       /*  For Java9+  (uses invokedynamic with java.lang.invoke.StringConcatFactory.makeConcatWithConstants); it is garbage-free and further optimizable by JVM
       0: ldc           #7                  // String  ňumba
       2: iconst_2
       3: invokevirtual #9                  // Method java/lang/String.repeat:(I)Ljava/lang/String;
       6: invokedynamic #15,  0             // InvokeDynamic #0:makeConcatWithConstants:(Ljava/lang/String;)Ljava/lang/String;
         */

        assertThat(helloClass, equalTo("vatapa ňumba ňumba"));
    }
}
