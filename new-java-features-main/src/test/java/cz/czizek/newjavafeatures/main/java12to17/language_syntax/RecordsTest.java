package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.junit.jupiter.api.Test;

import static java.lang.reflect.Modifier.isPrivate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RecordsTest {


    @Test
    void recordHasGeneratedCode() {

        record Point(int x, int y) {
        }

        // constructor
        var point = assertDoesNotThrow(() -> new Point(7, 8));

        // private fields
        var xField = assertDoesNotThrow(() -> Point.class.getDeclaredField("x"));
        var yField = assertDoesNotThrow(() -> Point.class.getDeclaredField("y"));

        assertThat(xField.getName(), is(equalTo("x")));
        assertThat(isPrivate(xField.getModifiers()), is(true));

        assertThat(yField.getName(), is(equalTo("y")));
        assertThat(isPrivate(yField.getModifiers()), is(true));

        // accessor methods
        assertThat(point.x(), is(7));
        assertThat(point.y(), is(8));

        // equals and hashCode
        assertThat(point.hashCode(), is(notNullValue()));
        assertThat(point.equals(new Point(7, 8)), is(true));

        // toString
        assertThat(point.toString(), is(equalTo("Point[x=7, y=8]")));
    }

    @Test
    void canHaveCompactCanonicalConstructor() {
        record Employee(String name, double wage) {
            Employee {
                // no fields assignment in a compact canonical constructor
                // The compact form helps developers focus on validating and normalizing parameters without the tedious work of assigning parameters to fields.

                if (wage > 1_000_000) throw new IllegalArgumentException("Too expensive!");

                // arguments (which are generated automatically from the record header) can be changed
                name = name.toUpperCase();

                // values are assigned to the private fields automatically at the end of constructor body
            }
        }

        assertThat(new Employee("someone", 1_000_000).name(), is(equalTo("SOMEONE")));
        assertThrows(IllegalArgumentException.class, () -> new Employee("someone", 2_000_000));
    }
}
