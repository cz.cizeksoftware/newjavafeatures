package cz.czizek.newjavafeatures.main.java8to11.features;

import cz.czizek.newjavafeatures.domain.food.BigMac;
import cz.czizek.newjavafeatures.domain.realestate.House;
import org.junit.jupiter.api.Test;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.invoke.VarHandle;
import java.util.Objects;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MethodAndVariableHandlesTest {
    private static class Person {
        String name;
        long age;

        public Person(String name, long age) {
            this.name = name;
            this.age = age;
        }

        @Override
        public String toString() {
            return name + ", aged " + age;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof Person op && this.age == op.age && Objects.equals(this.name, op.name); // note: instanceof operator pattern matching is supported since Java16
        }
    }

    @Test
    void methodHandles() throws Throwable {
        /////////////////////////////
        // We already know Method Handles from Java7:
        Lookup lookup = MethodHandles.lookup();
        MethodType methodSignature = MethodType.methodType(String.class);  // general signature of a (any) method with no input parameters and returning String
        MethodHandle toStringMethodHandle = lookup.findVirtual(Person.class, "toString", methodSignature); // general "caller" to a  toString  method of given signature on Person type

        Person person = new Person("Henry", 50L);
        String returnedString = (String) toStringMethodHandle.invoke(person); // in runtime, the same as   (String) ((Person) person).toString()   but determined only during runtime, not in compile time
        assertThat(returnedString, equalTo("Henry, aged 50"));

    }

    @Test
    void methodHandleCanBeTransformed() throws Throwable {
        MethodHandle personConstructor = MethodHandles.lookup().findConstructor(Person.class, MethodType.methodType(void.class, String.class, long.class));
        Person alice1st = (Person) personConstructor.invoke("Alice", 30L);
        assertThat(alice1st, is(equalTo(new Person("Alice", 30L))));

        // transform a MethodHandle accepting individual arguments into a MH accepting an array of arguments
        MethodHandle personConstructorAcceptingArray = personConstructor.asSpreader(Object[].class, 2);
        Object[] arguments = {"Alice", 30L};
        Person alice2nd = (Person) personConstructorAcceptingArray.invoke(arguments);
        assertThat(alice2nd, is(equalTo(alice2nd)));

        // transforms a MH accepting a String and a long to a MH accepting a String and an Integer
        MethodHandle constructorWithDifferentTypes = personConstructor.asType(MethodType.methodType(Person.class, String.class, Integer.class));
        Person bob = (Person) constructorWithDifferentTypes.invoke("Bob", 40);
        assertThat(bob, is(equalTo(new Person("Bob", 40L))));


        // transform MH that accepts an instance to MH bound to a specific instance
        MethodHandle generalToStringOnPerson = MethodHandles.lookup().findVirtual(Person.class, "toString", MethodType.methodType(String.class));
        MethodHandle toStringOnAlice1st = generalToStringOnPerson.bindTo(alice1st);
        assertThat(toStringOnAlice1st.invoke(), is(equalTo(alice1st.toString())));
    }

    @Test
    void variableHandles() throws NoSuchFieldException, IllegalAccessException {
        // Now we have a similar thing for variables - Variable Handles
        // Note: The goal of VarHandle is to define a standard for invoking
        //       equivalents of java.util.concurrent.atomic and sun.misc.Unsafe
        //       operations on fields and array elements.

        Person person = new Person("Henry", 50L);

        VarHandle ageHandle = MethodHandles.lookup().findVarHandle(Person.class, "age", long.class);
        long age = (long) ageHandle.get(person); // in runtime, the same as  (long) ((Person) person).age   !accesses the field, not any getter!
        assertThat(age, equalTo(50L));


        ageHandle.set(person, 40L); // yes, __atomically__ sets long (or double) value !

        ageHandle.setVolatile(person, 30L); // atomically sets the value as if the field was declared  volatile  no matter if it actually is volatile or not
        assertThat(ageHandle.getVolatile(person), is(30L)); // gets the value as if the field was declared  volatile  no matter if it actually is volatile or not

        // performs compareAndSet of the value as if the field was declared volatile
        boolean wasTheValueChanged = ageHandle.compareAndSet(person, 40L, 20L);
        assertThat(wasTheValueChanged, is(false));

        assertThat(person.toString(), equalTo("Henry, aged 30"));
    }


    // See  module-info.java  of module  new-java-features-domain  to see which packages are exported/open
    @Test
    void publicMembersOfOtherModuleAreAccessible() throws NoSuchFieldException, IllegalAccessException {
        BigMac bigMac = new BigMac();
        VarHandle priceHandle = MethodHandles.lookup().findVarHandle(BigMac.class, "price", double.class);
        double price = (double) priceHandle.get(bigMac);
        assertThat(price, is(0.0));
    }

    @Test
    void privateMembersOfOtherModuleAreNotAccessibleIfNotOpen() {
        assertThrows(IllegalAccessException.class, () -> MethodHandles.privateLookupIn(BigMac.class, MethodHandles.lookup()));
    }

    @Test
    void privateMembersOfOtherModuleAreAccessibleIfOpen() throws NoSuchFieldException, IllegalAccessException {
        //  privateLookupIn  MUST be used to access private members

        House house = new House();

        // Module of House class must not only export the package but also must OPEN it
        final Lookup privateLookup = assertDoesNotThrow(() -> MethodHandles.privateLookupIn(House.class, MethodHandles.lookup()));
        VarHandle priceHandle = privateLookup.findVarHandle(House.class, "price", double.class);

        double price = (double) priceHandle.get(house);
        assertThat(price, is(0.0));

        priceHandle.set(house, 1_000_000.0);
        assertThat(house.getPrice(), equalTo(1_000_000.0));
    }
}
