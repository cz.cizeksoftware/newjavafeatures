package cz.czizek.newjavafeatures.main.java8to11.features;

import cz.czizek.newjavafeatures.domain.food.BigMac;
import cz.czizek.newjavafeatures.domain.realestate.House;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InaccessibleObjectException;
import java.lang.reflect.Modifier;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class JavaPlatformModuleSystemTest {

    /*
    See  module-info.java

    In-module testing
    * we use  junit-platform-maven-plugin  instead of  maven-surefire-plugin  because it enables in-module testing by:
      * it supports  module-info.test  file that effectively amends this module declaration (module-info.java)
        with additional directives (using standard  javac  options) that enable testing framework (JUnit) to be used in this module
        without a need to declare these test-specific directives in main-code  module-info.java
      * and it "merges" main and test code to a single module for the time of running tests ( --patch-module  option of  java  command)
    * there are other options, incl. extra-module testing, of course. See https://sormuras.github.io/blog/2018-09-11-testing-in-the-modular-world.html
     */

    @Test
    void nonExportedPackageIsNotAccessible() throws ClassNotFoundException {
        final Class<?> tortoiseClass = this.getClass().getClassLoader().loadClass("cz.czizek.newjavafeatures.domain.animals.Tortoise");
        assertThrows(IllegalAccessException.class, () -> tortoiseClass.getConstructor().newInstance());
    }

    @SuppressWarnings("Convert2MethodRef")
    @Test
    void exportedPackageIsAccessibleIfRequired() throws NoSuchFieldException {
        assertDoesNotThrow(() -> new BigMac());
        final Field privateCaloriesField = BigMac.class.getDeclaredField("calories");
        assertThat(Modifier.isPrivate(privateCaloriesField.getModifiers()), is(true));
        assertThrows(InaccessibleObjectException.class, () -> privateCaloriesField.setAccessible(true));
    }

    @Test
    void exportedAndOpenPackageHasAccessibleEvenPrivateMembers() throws NoSuchFieldException {
        House house = new House();
        final Field privatePriceField = House.class.getDeclaredField("price");
        assertThat(Modifier.isPrivate(privatePriceField.getModifiers()), is(true));
        assertDoesNotThrow(() -> privatePriceField.setAccessible(true));
        assertDoesNotThrow(() -> privatePriceField.get(house));
    }
}
