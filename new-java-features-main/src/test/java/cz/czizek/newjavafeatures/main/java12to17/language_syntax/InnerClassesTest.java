package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class InnerClassesTest {
    @Test
    void innerClassesCanHaveStaticMembers() {
        // before Java16, inner classes could only have static members if they were constant variables

        assertDoesNotThrow(() -> Inner.nonConstant = "runtime assigned value");
    }

    class Inner {
        static final String CONSTANT = "constant value"; // only this was possible before Java16
        static String nonConstant; // This is only possible since Java16
    }
}
