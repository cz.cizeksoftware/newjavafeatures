package cz.czizek.newjavafeatures.main.java8to11.api_changes;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;

public class DeprecatedAnnotationTest {
    @Test
    void enhancedDeprecatedAnnotation_has_since_and_forRemoval_attributes() {
        // Enhanced  @Deprecated  annotation  (new  since  and  forRemoval  attributes)
        List<String> deprecatedAnnotationAttributes =
                Stream.of(Deprecated.class.getMethods())
                        .map(Method::getName)
                        .collect(toList());
        assertThat(deprecatedAnnotationAttributes, allOf(hasItem("since"), hasItem("forRemoval")));
    }
}
