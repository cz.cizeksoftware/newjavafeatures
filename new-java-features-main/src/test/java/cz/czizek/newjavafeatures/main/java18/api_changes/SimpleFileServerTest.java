package cz.czizek.newjavafeatures.main.java18.api_changes;

import com.sun.net.httpserver.SimpleFileServer;
import org.junit.jupiter.api.Test;

import java.net.InetSocketAddress;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SimpleFileServerTest {
    @Test
    void staticHttpServerServesFiles() throws Exception {

        Path tempDir = Files.createTempDirectory("tempDir");
        Path filePath1 =
                Files.writeString(
                        Files.createTempFile(tempDir,
                                             "file1",
                                             ".txt"),
                        "teststring");

        var address = new InetSocketAddress(8080);
        var server = SimpleFileServer
                .createFileServer(address,
                                  tempDir,
                                  SimpleFileServer.OutputLevel.VERBOSE);
        server.start();


        var request = HttpRequest
                .newBuilder()
                .uri(new URI("http://%s:%s/%s"
                                     .formatted(address.getHostName(),
                                                address.getPort(),
                                                filePath1.getFileName())))
                .GET()
                .build();
        var response = HttpClient
                .newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());

        assertThat(response.body(), is(equalTo("teststring")));

    }
}
