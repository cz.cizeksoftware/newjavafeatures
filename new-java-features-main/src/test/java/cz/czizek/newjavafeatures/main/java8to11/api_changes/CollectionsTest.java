package cz.czizek.newjavafeatures.main.java8to11.api_changes;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CollectionsTest {

    @Test
    public void factoryMethodsForCollections() {
        // Factory methods in Collections

        // any number of items for List or Set
        List<String> languages = List.of("Java", "Kotlin");
        assertThat(languages, contains("Java", "Kotlin"));

        // max. 10 pairs for Map
        Map<String, String> languageClassification = Map.of(
                "Nightmare", "PLSQL",
                "Reality", "Java8",
                "Dream", "Kotlin");
        assertThat(languageClassification.get("Nightmare"), equalTo("PLSQL"));
        assertThat(languageClassification.get("Reality"), equalTo("Java8"));
        assertThat(languageClassification.get("Dream"), equalTo("Kotlin"));

        // any number of pairs for Map
        Map<String, String> languageClassification2 = Map.ofEntries(
                Map.entry("Nightmare", "PLSQL"),
                Map.entry("Reality", "Java8"),
                Map.entry("Dream", "Kotlin"));

        assertThat(languageClassification2.get("Nightmare"), equalTo("PLSQL"));
        assertThat(languageClassification2.get("Reality"), equalTo("Java8"));
        assertThat(languageClassification2.get("Dream"), equalTo("Kotlin"));

    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void immutableCopyOfCollection() {
        // immutable copy of a collection
        List<String> l = new ArrayList<>(); // a mutable list
        l.add("A");

        assertThat(l, contains("A"));

        List<String> immutableCopy = List.copyOf(l); // an immutable one
        assertThrows(UnsupportedOperationException.class, () -> immutableCopy.add("B"));

        l.add("B");
        assertThat(immutableCopy, contains("A"));
    }

    @SuppressWarnings("Convert2MethodRef")
    @Test
    void toArrayWithGeneratorFunctionGivenCollectionSize() {
        String[] stringArray = List.of("A", "B", "C").toArray(collectionSize -> new String[collectionSize]);

        assertThat(stringArray, is(arrayContaining("A", "B", "C")));
    }
}
