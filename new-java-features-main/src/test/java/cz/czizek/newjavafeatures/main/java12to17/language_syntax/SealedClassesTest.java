package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SealedClassesTest {

    // sealed hierarchy declaration using  permits  clause
    sealed class A permits B, C {
    }

    final class B extends A {
    }

    non-sealed class C extends A {
    }

    // sealed hierarchy declaration using nested classes (note that Records are implicitly final so we can omit the  final  modifier)
    sealed interface Expr {
        record ConstantExpr(int i) implements Expr {
        }

        record PlusExpr(Expr a, Expr b) implements Expr {
        }

        record TimesExpr(Expr a, Expr b) implements Expr {
        }

        record NegExpr(Expr e) implements Expr {
        }
    }

    @Test
    void doesntPreventTypeConversionWhenANonSealedClassIsInHierarchy() {
        Assertions.assertThrows(ClassCastException.class, () -> {
            // would be a COMPILE-TIME error if the sealed type hierarchy didn't contain a non-sealed class (or if A was a final class)
            Runnable typedToAnything = (Runnable) new A();
        });
    }
}
