package cz.czizek.newjavafeatures.main.java8to11.language_syntax;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TryWithResourcesTest {

    @SuppressWarnings("EmptyTryBlock")
    @Test
    void tryWithResourcesCanNowUsePredeclaredVariables() {
        AtomicBoolean hasBeenClosed = new AtomicBoolean(false);

        //////////////////////
        // try-with-resources  now doesn't have to declare its own variables, it can use already existing final or effectively final variable from enclosing context
        AutoCloseable ac = () -> {
            // .close() method
            hasBeenClosed.set(true);
        };

        try (ac) {
            // do some stuff with ac
        } catch (Exception e) {
            System.err.println("Closing the AutoCloseable threw " + e);
        }

        assertTrue(hasBeenClosed.get());
    }
}
