package cz.czizek.newjavafeatures.main.java12to17.api_changes;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class StringTest {
    @Test
    void stringIndentMethod_addsOrRemovesIndentation() {
        // String    .indent()  method
        String text = "  Hello Java!\nThis is an example of .indent() method.";
        String indentedText = text.indent(4);

        // Note the added trailing  line-end  !
        assertThat(indentedText, equalTo("      Hello Java!\n    This is an example of .indent() method.\n"));

        String unindentedText = indentedText.indent(-10); // only takes available whitespace characters, doesn't impact the text itself
        assertThat(unindentedText, equalTo("Hello Java!\nThis is an example of .indent() method.\n"));
    }

    @SuppressWarnings("Convert2MethodRef")
    @Test
    void stringTransformMethod_acceptsFunctionToTransformTheString() {
        // String   .transform()  method
        final var textLength = "Hello Java!".transform(string -> string.length());
        assertThat(textLength, equalTo(11));
    }

    @Test
    void stringFormattedMethod() {
        // Allows calling equivalent of String.format() directly on an instance of String
        var str1 = "Hello %s".formatted("Mister");
        var str2 = String.format("Hello %s", "Mister");

        // works for Text Blocks too
        var str3 = """
                Hello %s""".formatted("Mister");

        assertThat(str1, equalTo("Hello Mister"));
        assertThat(str1, equalTo(str2));
        assertThat(str1, equalTo(str3));
    }

}
