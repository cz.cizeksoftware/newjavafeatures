package cz.czizek.newjavafeatures.main.java12to17.features;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class NullPointerExceptionTest {
    @Test
    void helpfulNullPointerExceptions() {
        var home = new House();

        Throwable thrownException = assertThrows(NullPointerException.class, () -> {
            var firstPieceOfFurniture = home.rooms.get(0).furnitures.get(0).name;
        });

        assertThat(thrownException.getMessage(), containsString("Cannot invoke \"java.util.List.get(int)\" because \"home.rooms\" is null"));
        assertThat(thrownException.toString(), containsString("java.lang.NullPointerException: Cannot invoke \"java.util.List.get(int)\" because \"home.rooms\" is null"));

        /*
            On Java8 this results in:
            assertThat(thrownException.getMessage(), is(nullValue()));
            assertThat(thrownException.toString(), containsString("java.lang.NullPointerException"));
        */
    }

    private static class House {
        public List<Room> rooms;
    }

    private static class Room {
        public List<Furniture> furnitures;
    }

    private static class Furniture {
        public String name;
    }
}
