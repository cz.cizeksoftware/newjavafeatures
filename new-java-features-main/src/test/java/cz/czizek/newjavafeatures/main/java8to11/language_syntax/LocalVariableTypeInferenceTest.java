package cz.czizek.newjavafeatures.main.java8to11.language_syntax;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class LocalVariableTypeInferenceTest {
    @Test
    void typeOfLocalVariableIsInferred() {

        /////////////////////
        // Local Variable Type Inference; Take a look at Style Guidelines (http://openjdk.java.net/projects/amber/LVTIstyle.html) for when (not) to use it

        // instead of e.g.
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        // consider ...
        var outputStream2 = new ByteArrayOutputStream();

        assertThat(variableTypeOf(outputStream2), equalTo(ByteArrayOutputStream.class));


        // We can still make the variable final
        final var str = "A"; // final variable
    }

    @Test
    void typeOfTryWithReaourcesVariableIsInferred() {
        // also in try-with-resources
        try (var emptyCloseable = new MyCloseable()) {
            // do some stuff
            assertThat(emptyCloseable.getClass(), equalTo(MyCloseable.class));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void typeOfLambdaParameterIsInferred() {
        // also in lambda
        Stream.of("Java", "Kotlin")
                .forEach((@Deprecated var x) -> {
                            assertThat(x.getClass(), equalTo(String.class));
                        }
                );
    }

    Class<?> variableTypeOf(ByteArrayOutputStream variable) {
        return ByteArrayOutputStream.class;
    }

    Class<?> variableTypeOf(Object variable) {
        return Object.class;
    }

    public static class MyCloseable implements AutoCloseable {
        @Override
        public void close() {

        }
    }
}
