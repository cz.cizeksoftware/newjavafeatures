package cz.czizek.newjavafeatures.main.java12to17.api_changes;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class FilesTest {
    @Test
    void filesMismatchMethod_tellsWhereTwoFilesStartToDiffer() throws IOException {
        // Files    .mismatch() method
        Path filePath1 = Files.createTempFile("file1", ".txt");
        Path filePath2 = Files.createTempFile("file2", ".txt");
        Path filePath3 = Files.createTempFile("file3", ".txt");
        Files.writeString(filePath1, "Some file content");
        Files.writeString(filePath2, "Some file content");
        Files.writeString(filePath3, "Some other file content");

        assertThat(Files.mismatch(filePath1, filePath2), equalTo(-1L)); // files are equal
        assertThat(Files.mismatch(filePath1, filePath3), equalTo(5L)); // files differ on 5th byte (counting from 0)
    }
}
