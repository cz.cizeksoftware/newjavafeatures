package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ValueBasedClassesTest {
    /*
    Many JDK API classes have been designated as Value-Based.
    Some future Java version will change their behaviour in terms of identity.

    Note: This, technically, doesn't belong to language_syntax section yet, because right now, nothing really changes.
          However, these classes ought not be used in a non-value-based-classes-compatible way because their treatment will change in the future.
     */

    @Test
    void valueBasedClassesAreMarked() {
        // Note: many more classes are marked as Value-Based - see usages of ValueBased annotation
        var someValueBasedClasses = Stream.of(
                Integer.class,
                Byte.class,
                Short.class,
                Long.class,
                Float.class,
                Double.class,
                Boolean.class,
                Character.class,
                Optional.class,
                Duration.class,
                LocalDateTime.class);

        boolean allAreMarkedAsValueBased =
                someValueBasedClasses
                        .map(Class::getAnnotations)
                        .map(Stream::of)
                        .allMatch(classAnnotations -> classAnnotations
                                .map(Annotation::annotationType)
                                .map(Class::getName)
                                .anyMatch("jdk.internal.ValueBased"::equals));

        assertThat(allAreMarkedAsValueBased, is(true));
    }
}
