package cz.czizek.newjavafeatures.main.java20.language_syntax;


import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SwitchTest {

    @Test
    void patternMatchingInSwitch() {
        Object o = "a";
        var result = switch (o) {
            case Integer i -> "integer " + i;
            case String s when s.startsWith("b") -> "string starting with \"b\" " + s;
            case String s -> "string " + s;
            default -> "default";
        };
        assertThat(result, is("string a"));
    }

    @Test
    void anyTypeInSwitch() {
        record Person(String name, int age) {
        }
        Person person = new Person("Pavel", 40);
        var result = switch (person) {
            case Person p when p.age > 40 -> "old person";
            case Person p -> "young person";
            //    default is not needed as selector expression is of type Person
            //    and 2nd pattern label matches any Person, the switch is exhaustive
        };

        assertThat(result, is("young person"));
    }

    @Test
    void nullAndDefaultLabels() {
        String o = null;
        assertThrows(NullPointerException.class, () -> {
            switch (o) {
                case "A" -> { /* some code here */ }
                default -> { /* no-op default */ }
            }
        });

        var result = switch (o) {
            case "A" -> "a";
            case null -> "n";
            default -> "d";
        };
        assertThat(result, is("n"));

        result = switch (o) {
            case "A" -> "a";
            case null, default -> "nothing";
        };
        assertThat(result, is("nothing"));
    }
}
