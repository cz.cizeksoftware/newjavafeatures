package cz.czizek.newjavafeatures.main.java8to11.language_syntax;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InterfacesTest {
    @Test
    void interfaceContainsPrivateMethods() {
        ////////////////////////////
        // private methods in interfaces
        // reason: to be able to split long  default  methods

        boolean hasPrivateMethod =
                Stream.of(TimeTelling.class.getDeclaredMethods())
                        .map(Method::getModifiers)
                        .anyMatch(Modifier::isPrivate);
        assertTrue(TimeTelling.class.isInterface() && hasPrivateMethod);

    }

    public interface TimeTelling {
        private String asString(LocalDate date) {
            return date.toString();
        }

        private String asString(LocalTime time) {
            return time.toString();
        }

        default String timeAsString(LocalDate date, LocalTime time) {
            return asString(time) + " of day " + asString(date); // long complex default method can now be split to private methods
        }
    }
}
