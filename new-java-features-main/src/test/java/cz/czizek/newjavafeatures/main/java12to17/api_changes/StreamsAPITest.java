package cz.czizek.newjavafeatures.main.java12to17.api_changes;

import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class StreamsAPITest {
    @Test
    void teeingCollector_collectsStreamByTwoCollectorsAndMergesResults() {
        //  Teeing Collector - a composite of two collectors
        double mean = Stream.of(1, 2, 3, 4, 5)
                .collect(
                        Collectors.teeing(
                                Collectors.summingDouble(Integer::doubleValue), // first collector
                                Collectors.counting(),            // second collector
                                (sum, count) -> sum / count));    // merger - produces final result from results of the two individual collectors
        assertThat(mean, equalTo(3.0));
    }
}
