package cz.czizek.newjavafeatures.main.java8to11.api_changes;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class FilesTest {

    @Test
    void newFilesMethodsToReadAndWriteString() throws IOException {
        //////////////////////
        // Files.readString   .writeString
        Path filePath = Files.writeString(Files.createTempFile("demo", ".txt"), "Sample text");
        assertThat(Files.readString(filePath), equalTo("Sample text"));
    }
}
