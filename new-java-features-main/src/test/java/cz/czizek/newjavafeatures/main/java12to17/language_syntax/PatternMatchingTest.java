package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import cz.czizek.newjavafeatures.domain.food.BigMac;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("ConstantConditions")
public class PatternMatchingTest {


    /*
    Note: Read a memo on Pattern Matching for Java from 2018 by Brian Goetz : https://cr.openjdk.java.net/~briangoetz/amber/pattern-match.html
          Pattern Matching for  instanceof  operator is just 1st pattern type supported. More are planned in the future
     */

    @Test
    void extractsPatternVariableByInstanceofPattern() {
        Object obj = new BigMac();

        if (obj instanceof BigMac bigMac) {
            // pattern variable is already casted for us
            assertThat(bigMac.price, is(equalTo(0.0)));
        }
    }

    @Test
    void patternVariableIsInScopeWhereItMustHaveMatched() {
        // The motto is: "A pattern variable is in scope where it has definitely matched"

        Object obj = new BigMac();

        if (obj instanceof BigMac bigMac && bigMac.price == 0.0) {
            // obj instanceof BigMac bigMac || bigMac.price == 0.0   would be compilation error

            assertThat(bigMac.price, is(equalTo(0.0)));
        }

        if (!(obj instanceof BigMac sandwich)) throw new RuntimeException();

        // the pattern variable is in scope here because this line can be only reached if the  instanceof  evaluation was true
        assertThat(sandwich.price, is(equalTo(0.0)));
    }
}
