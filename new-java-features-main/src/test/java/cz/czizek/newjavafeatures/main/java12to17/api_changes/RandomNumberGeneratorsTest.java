package cz.czizek.newjavafeatures.main.java12to17.api_changes;

import org.junit.jupiter.api.Test;

import java.util.random.RandomGenerator;
import java.util.random.RandomGeneratorFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RandomNumberGeneratorsTest {

    @Test
    void varietyOfRandomNumberGeneratorsAreAvailable() {
        long generatorsAvailable = RandomGeneratorFactory.all().count();

        assertThat(generatorsAvailable, is(greaterThanOrEqualTo(13L)));
    }

    @Test
    void RandomGeneratorFactoryFindsARandomGeneratorImplementation() {
        RandomGenerator generator = RandomGeneratorFactory.of("L128X1024MixRandom").create();

        double aDouble = generator.nextDouble();
        double anotherDouble = generator.nextDouble();

        assertThat(aDouble, is(not(equalTo(anotherDouble))));
    }
}
