package cz.czizek.newjavafeatures.main.java8to11.api_changes;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StringTest {

    private final char EN_QUAD = '\u2000'; // 1 "n"-wide space which width does not change with expanding or condensing of the font (contrary to normal "n" space)

    @Test
    void repeatsString() {
        // .repeat()
        assertThat("La ".repeat(2) + "Land", equalTo("La La Land"));
    }

    @Test
    void splitsStringToLines() {
        // .lines()
        List<String> lines = "hello\nworld".lines().collect(Collectors.toList());
        assertThat(lines, contains("hello", "world"));
    }

    @Test
    void checksIfStringIsBlank() {
        // .isBlank()
        String blankString = "  " + EN_QUAD;
        assertThat(blankString.isBlank(), is(true));
    }

    @Test
    void stripsStringOfWhitespaces_inUnicodeAwareManner() {
        // .strip()  - Unicode-aware alter ago or  .trim()
        String string = EN_QUAD + "some text" + EN_QUAD;
        assertThat(string.trim(), equalTo(string));    // .trim()  doesn't remove unicode whitespace characters
        assertThat(string.strip(), equalTo("some text"));  // .strip()  does
    }
}
