package cz.czizek.newjavafeatures.main.java8to11.api_changes;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StreamsAPITest {

    @Test
    void takeWhileAndDropWhile() {
        // takeWhile
        List<Integer> afterTakeWhile = Stream.of(1, 9, 2, 8, 3, 7, 4, 6, 5)
                .takeWhile(i -> i != 4) // leaves stream of  1, 9, 2, 8, 3, 7
                .collect(toList());
        assertThat(afterTakeWhile, contains(1, 9, 2, 8, 3, 7));

        // dropWhile
        List<Integer> afterDropWhile = Stream.of(1, 9, 2, 8, 3, 7, 4, 6, 5)
                .dropWhile(i -> i != 7) // leaves stream of 7, 4, 6, 5
                .collect(toList());
        assertThat(afterDropWhile, contains(7, 4, 6, 5));
    }

    @Test
    void unboundedStreamsLimitedByPredicate() {
        // Infinite Stream, but not exactly infinite  - new  .iterate()  overload with  hasNext  predicate

        // !! We've already known this since Java8 - it creates an infinite stream! (we must use this in combination with .limit() ! )
        List<Integer> limitedTo5 = Stream
                .iterate(0,
                        n -> n + 2)
                .limit(5)
                .collect(toList());


        assertThat(limitedTo5, contains(0, 2, 4, 6, 8));

        // now we can limit the "infinite" stream by a Predicate and without using  .limit()  method
        List<Integer> limitedByPredicate = Stream
                .iterate(0,
                        i -> i < 10,  // hasNext  predicate
                        n -> n + 2)
                .collect(toList());

        assertThat(limitedByPredicate, contains(0, 2, 4, 6, 8));
    }

    @Test
    void streamOfNullable_producesStreamOf1ItemOrEmptyStream() {
        // .ofNullable()
        assertThat(Stream.ofNullable(null).collect(toList()), is(empty()));  // returns an empty stream
        assertThat(Stream.ofNullable(7).collect(toList()), contains(7));  // returns a stream with 1 item
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void toUnmodifiableCollectors() {
        // toUnmodifiable* collectors
        List<String> modifiable = Stream.of("A", "B").collect(Collectors.toList());
        List<String> unmodifiable = Stream.of("A", "B").collect(Collectors.toUnmodifiableList());

        assertDoesNotThrow(() -> modifiable.add("C"));
        assertThrows(UnsupportedOperationException.class, () -> unmodifiable.add("C"));

        assertThat(modifiable, hasItem("C"));
        assertThat(unmodifiable, not(hasItem("C")));
    }

    @Test
    void predicateNot() {
        //////////////////////
        // predicate  .not()
        List<String> letters = Stream.of("A", "B", "C")
                .filter(Predicate.not("B"::equals))
                .collect(toList());
        assertThat(letters, contains("A", "C"));
    }
}
