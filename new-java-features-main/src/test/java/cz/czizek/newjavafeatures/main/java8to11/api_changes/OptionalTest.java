package cz.czizek.newjavafeatures.main.java8to11.api_changes;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OptionalTest {

    @Test
    void newOverloadOfOrMethod_accepts_Supplier_to_produce_the_alternative_value() {
        String value = Optional.<String>empty().or(() -> Optional.of("Lord")).get(); // gets missing value from given  Supplier<Optional<T>>
        assertThat(value, equalTo("Lord"));
    }

    @Test
    void ifPresentOrElse() {
        // .ifPresentOrElse()
        AtomicReference<String> whatsItDone = new AtomicReference<>("nothing");
        Optional.empty()
                .ifPresentOrElse(
                        nameValue -> whatsItDone.set("handled name " + nameValue),  // does something with the value
                        () -> whatsItDone.set("handled empty Optional"));  // or does something else if the value is missing

        assertThat(whatsItDone.get(), equalTo("handled empty Optional"));

        Optional.of("Popelec Hadimrška")
                .ifPresentOrElse(
                        nameValue -> whatsItDone.set("handled name " + nameValue),  // does something with the value
                        () -> whatsItDone.set("handled empty Optional"));  // or does something else if the value is missing
        assertThat(whatsItDone.get(), equalTo("handled name Popelec Hadimrška"));
    }

    @Test
    void optionalToStreamConversion() {
        // .stream()
        Stream<Object> emptyStream = Optional.empty().stream();// converts Optional to Stream of 1 item or an empty Stream
        assertThat(emptyStream.count(), equalTo(0L));

        Stream<String> blablaStream = Optional.of("blabla").stream();
        assertThat(blablaStream.collect(toList()), contains("blabla"));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void isEmptyMethod() {
        // .isEmpty()
        assertTrue(Optional.empty().isEmpty()); // returns true;  alter-ego of  .isPresent()
    }
}
