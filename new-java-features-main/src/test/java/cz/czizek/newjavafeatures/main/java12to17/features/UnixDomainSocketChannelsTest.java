package cz.czizek.newjavafeatures.main.java12to17.features;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.StandardProtocolFamily;
import java.net.UnixDomainSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class UnixDomainSocketChannelsTest {

    private Path tempFile;

    @BeforeEach
    void init() {
        String tempDir = System.getProperty("java.io.tmpdir");
        tempFile = Path.of(tempDir, "tempFile");
    }

    /*
      For explanation of WHAT and WHY this is, see https://inside.java/2021/02/03/jep380-unix-domain-sockets-channels/

      Note: Unix Domain Socket Channels are, despite its name, supported also on Linux, MacOS and Windows 10+
     */
    @Test
    void communicatesOverUnixDomainSockets() throws IOException {
        try {
            var server = Server.createSocketChannel(tempFile);

            Client.send(tempFile, "Hello!");

            String serverReceivedMessage = server.listenForMessage();

            assertThat(serverReceivedMessage, is(equalTo("Hello!")));

        } finally {
            // Normally, Server is responsible for removing the file from filesystem when it closes the ServerSocketChannel!
            // I have it here just to make sure it gets removed even if the test fails
            Files.deleteIfExists(tempFile);
        }
    }

    private static class Client {
        static void send(Path tempFile, String message) throws IOException {
            UnixDomainSocketAddress address = UnixDomainSocketAddress.of(tempFile);
            // ServerSocketChannel must already be bound to the address for this (client)SocketChannel to be able to bind to it
            try (SocketChannel clientChannel = SocketChannel.open(address)) {
                ByteBuffer buf = StandardCharsets.UTF_8.encode(message);
                clientChannel.write(buf);
            }
        }
    }

    private static class Server {
        private final ServerSocketChannel serverChannel;

        private Server(ServerSocketChannel serverChannel) {
            this.serverChannel = serverChannel;
        }

        static Server createSocketChannel(Path tempFile) throws IOException {
            var address = UnixDomainSocketAddress.of(tempFile);
            var serverChannel = ServerSocketChannel.open(StandardProtocolFamily.UNIX);
            serverChannel.bind(address); // creates the file on filesystem
            return new Server(serverChannel);
        }

        String listenForMessage() throws IOException {
            try (serverChannel;
                 var clientChannel = serverChannel.accept()) { // blocks until Client has connected
                var buf = ByteBuffer.allocate(64);

                // by default blocks until some data are available
                clientChannel.read(buf);

                buf.flip();

                // Just for demonstration - this is not safe for multi-byte characters or \r\n line separators!
                return new String(StandardCharsets.UTF_8.decode(buf).array());
            }
        }
    }
}
