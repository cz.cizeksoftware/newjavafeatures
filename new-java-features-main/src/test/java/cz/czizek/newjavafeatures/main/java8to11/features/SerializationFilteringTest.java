package cz.czizek.newjavafeatures.main.java8to11.features;

import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.stream.IntStream;

import static java.io.ObjectInputFilter.Status.REJECTED;
import static java.io.ObjectInputFilter.Status.UNDECIDED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SerializationFilteringTest {

    // see https://docs.oracle.com/en/java/javase/16/core/serialization-filtering1.html for Serialization Filtering description and more filtering options
    // Note: Java17 comes with Context-Specific Deserialization Filters - see https://openjdk.java.net/jeps/415 for description and example


    static class Person implements Serializable {
        String name = "Paul";
        Object[] loanContracts = {};
    }

    static class House implements Serializable {
        int stories = 7;
    }

    @Test
    void deserializesObject() throws IOException, ClassNotFoundException {

        Person person = new Person();
        person.loanContracts = IntStream.range(1, 3)
                .mapToObj(i -> "loan for " + i + "k")
                .toArray();

        byte[] bytes;
        try (var baos = new ByteArrayOutputStream();
             var oos = new ObjectOutputStream(baos)) {
            oos.writeObject(person);
            bytes = baos.toByteArray();
        }

        Person readPerson;

        try (var bais = new ByteArrayInputStream(bytes);
             var ois = new ObjectInputStream(bais)) {
            readPerson = (Person) ois.readObject();
        }

        assertThat(person.name, is(equalTo(readPerson.name)));
    }

    @Test
    void preventsDeserializationOfCertainClass() throws IOException {

        // ! Note that there is a VM option set to prevent deserialization of type  House:
        //  -Djdk.serialFilter=!cz.czizek.newjavafeatures.main.java8to11.features.SerializationFilteringTest$House
        //  (see it set in  maven-surefire-plugin's  configuration)
        // Default  ObjectInputFilter  reads that and filters all attempts to deserialize an object

        byte[] bytes;
        try (var baos = new ByteArrayOutputStream();
             var oos = new ObjectOutputStream(baos)) {
            House house = new House();
            oos.writeObject(house);
            bytes = baos.toByteArray();
        }

        try (var bais = new ByteArrayInputStream(bytes);
             var ois = new ObjectInputStream(bais)) {
            var exception = assertThrows(InvalidClassException.class, () -> ois.readObject());
            assertThat(exception.getMessage(), is("filter status: REJECTED"));
        }
    }

    @Test
    void usesCustomObjectInputFilter() throws IOException {

        byte[] bytes;

        try (var baos = new ByteArrayOutputStream();
             var oos = new ObjectOutputStream(baos)) {
            Person person = new Person();

            // simulating malicious input:
            person.loanContracts = IntStream.range(1, 100) // imagine 100_000_000 items here
                    .mapToObj(i -> "loan for " + i + "k")
                    .toArray();
            oos.writeObject(person);
            bytes = baos.toByteArray();
        }


        // filter to refuse to deserialize objects with too many items in array
        ObjectInputFilter filter = filterInfo -> filterInfo.arrayLength() > 50 ? REJECTED : UNDECIDED;

        try (var bais = new ByteArrayInputStream(bytes);
             var ois = new ObjectInputStream(bais)) {
            ois.setObjectInputFilter(filter);
            var exception = assertThrows(InvalidClassException.class, () -> ois.readObject());
            assertThat(exception.getMessage(), is("filter status: REJECTED"));
        }
    }
}

