package cz.czizek.newjavafeatures.main.java8to11.features;

import org.junit.jupiter.api.Test;

import java.util.ResourceBundle;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class PropertyFilesTest {
    @Test
    void utf8PropertyFiles() {
        ////////////////
        // UTF-8 property files
        // Returns  Здравствуйте!  under Java9+ but  ÐÐ´ÑÐ°Ð²ÑÑÐ²ÑÐ¹ÑÐµ!  under Java8
        assertThat(ResourceBundle.getBundle("application").getString("greeting"), equalTo("Здравствуйте!"));
    }
}
