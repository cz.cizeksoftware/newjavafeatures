package cz.czizek.newjavafeatures.main.java8to11.features;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class NewHttpClientTest {
    @Test
    void newHttpClient() throws URISyntaxException, IOException, InterruptedException {
        /////////////////////////
        // new HTTP/2-compatible HTTP client
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("http://worldclockapi.com/api/json/utc/now"))
                .GET()
                .build();

        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());

        assertThat(response.statusCode(), equalTo(200));
        assertThat(response.body(), is(not(emptyOrNullString())));
    }
}
