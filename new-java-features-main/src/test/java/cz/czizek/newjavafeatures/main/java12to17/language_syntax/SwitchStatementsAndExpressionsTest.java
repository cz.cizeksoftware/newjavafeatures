package cz.czizek.newjavafeatures.main.java12to17.language_syntax;

import org.junit.jupiter.api.Test;

import static cz.czizek.newjavafeatures.main.java12to17.language_syntax.SwitchStatementsAndExpressionsTest.Towns.LONDON;
import static java.time.DayOfWeek.TUESDAY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({"EnhancedSwitchMigration", "ConstantConditions"})
class SwitchStatementsAndExpressionsTest {

    @Test
    void moreLabelsInOneCaseBranchInOldSwitchSyntax() {
        boolean isWeekDay;
        switch (TUESDAY) {
            // Classical "old" switch syntax, only now with more labels possible in one case branch
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY:
                isWeekDay = true;
                break; // We still need break here !
            case SATURDAY:
            case SUNDAY:
                isWeekDay = false;
                break;
            default:
                throw new IllegalArgumentException("There is no other day but I still must ensure isWeekDay doesn't get used uninitialized!");
        }

        assertTrue(isWeekDay);
    }

    @Test
    void newSwitchStatementSyntax() {
        // Old syntax as we know it
        boolean isWeekDay;
        switch (TUESDAY) {
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                isWeekDay = true;
                break;
            case SATURDAY:
            case SUNDAY:
                isWeekDay = false;
                break;
            default:
                throw new IllegalArgumentException("There is no other day but I still must ensure isWeekDay doesn't get used uninitialized!");
        }


        // New Syntax
        boolean isWeekDay2;
        switch (TUESDAY) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> isWeekDay2 = true; // no break needed, never "flows" to following cases
            case SATURDAY, SUNDAY -> isWeekDay2 = false;
            default -> throw new IllegalArgumentException("There is no other day but I still must ensure isWeekDay doesn't get returned uninitialized!");
        }


        assertThat(isWeekDay, equalTo(isWeekDay2));
    }

    @SuppressWarnings("RedundantLabeledSwitchRuleCodeBlock")
    @Test
    void switchIsAnExpression() {

        // Switch can be used as an "EXPRESSION" - i.e. it can return a value
        var isWeekDay = switch (TUESDAY) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> true;
            case SATURDAY, SUNDAY -> false;
        };

        var isWeekDay2 = switch (TUESDAY) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> {
                // some more complex logic
                yield true; // yield = return this value as a result of switch expression;  return  would mean to return from method
            }
            case SATURDAY, SUNDAY -> false;
        };

        assertTrue(isWeekDay);
        assertThat(isWeekDay, equalTo(isWeekDay2));
    }

    @SuppressWarnings("java:S2699")
    @Test
    void compilerEvaluatesExhaustiveness() {

        // For switch used as an expression (i.e. returning value), all possible values of input must be exhausted.
        // I.e. There must be output value for every possible input OR there must be default branch.
        // Compiler can check exhaustiveness for Enums.
        // In the future, compiler will also be able to check exhaustiveness of pattern-matching on Java17's Sealed Classes
        //
        //
        // For switch as a statement (i.e. not returning value), no exhaustiveness is enforced - no logic is executed for unknown input.

        var isWeekDayFromExhaustiveExpression = switch (TUESDAY) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> true;
            case SATURDAY, SUNDAY -> false;
        };

        var isWeekDayFromExpressionWithDefaultBranch = switch (TUESDAY) {
            case SATURDAY, SUNDAY -> false;
            default -> true;
        };

        switch (TUESDAY) { // switch statement doesn't have to be exhaustive
            case SATURDAY, SUNDAY -> System.out.println("Hooray!");
        }
    }

    @Test
    void switchIsAPolyexpression() {
        //
        // Similar to Lambdas, switch expressions are polymorphic expressions (polyexpressions)
        // I.e. they don't have a type of their own. Their type must be inferred from context (e.g. determined by target type)
        // I.e. if the left-hand side has a specific type then each
        //      branch of switch expression has to match that type
        //
        // switch expressions can be used with local type-inferred variables (var).
        // Type is then determined as the most specific (super)type that is shared by all switch branches

        var town = switch (LONDON) {
            case PARIS -> new FrenchTown();
            case LONDON -> new EnglishTown();
        };


        // All switch branches return Town => switch expression's type is Town
        assertThat(variableTypeOf(town), is(Town.class));

    }

    Class<?> variableTypeOf(Town town) {
        return Town.class;
    }

    Class<?> variableTypeOf(EnglishTown town) {
        return EnglishTown.class;
    }

    Class<?> variableTypeOf(FrenchTown town) {
        return FrenchTown.class;
    }

    static class Town {
    }

    static class FrenchTown extends Town {
    }

    static class EnglishTown extends Town {
    }

    enum Towns {PARIS, LONDON}
}
