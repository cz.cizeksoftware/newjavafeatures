package cz.czizek.newjavafeatures.main.java8to11.language_syntax;

import org.junit.jupiter.api.Test;

public class AnonymousInnerClassesTest {
    @Test
    void diamondOperatorForAnonymousInnerClasses() {
        ////////////////////
        // Diamond operator can be used even for anonymous inner classes
        FooClass<Integer> fc = new FooClass<>(1) {    // anonymous inner class; couldn't have used a diamond in prior Java versions
        };
        // could as well be FooClass<?> or FooClass<? extends Number> , ...
    }

    public static class FooClass<T> implements AutoCloseable {
        private final Integer i;

        public FooClass(Integer i) {
            this.i = i;
        }

        @Override
        public void close() {

        }
    }
}
