package cz.czizek.newjavafeatures.main.java12to17.api_changes;

import org.junit.jupiter.api.Test;

import java.text.CompactNumberFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

public class NumberFormatTest {
    @Test
    void newCompactNumberFormat() {
        // new CompactNumberFormat
        NumberFormat shortFormat = NumberFormat.getCompactNumberInstance(Locale.of("en", "US"), NumberFormat.Style.SHORT);

        assertThat(shortFormat, is(instanceOf(CompactNumberFormat.class)));

        shortFormat.setMaximumFractionDigits(2);
        assertThat(shortFormat.format(2592), equalTo("2.59K"));

        NumberFormat longFormat = NumberFormat.getCompactNumberInstance(Locale.of("en", "US"), NumberFormat.Style.LONG);
        longFormat.setMaximumFractionDigits(2);
        assertThat(longFormat.format(2592), equalTo("2.59 thousand"));
    }

}
