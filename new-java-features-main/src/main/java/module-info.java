module cz.czizek.newjavafeatures.main {
    requires cz.czizek.newjavafeatures.domain;
    requires java.net.http;
    requires jdk.httpserver;
}