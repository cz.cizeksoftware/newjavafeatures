module cz.czizek.newjavafeatures.domain {
    exports cz.czizek.newjavafeatures.domain.food;

    exports cz.czizek.newjavafeatures.domain.realestate;
    opens cz.czizek.newjavafeatures.domain.realestate;
}